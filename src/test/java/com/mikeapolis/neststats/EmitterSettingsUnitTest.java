package com.mikeapolis.neststats;

import com.mikeapolis.neststats.EmitterSettings;
import org.junit.Test;

import java.util.Properties;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class EmitterSettingsUnitTest {

    @Test
    public void get_theSettingThatWasSet_returnsCorrectSetting() {
        String testKey = "fakeKey";
        String testValue = "fakeValue";
        EmitterSettings testSettings = spy(EmitterSettings.getInstance());
        testSettings.set(testKey, testValue);

        String actualValue = testSettings.get(testKey);

        verify(testSettings).get(testKey);
        assertThat(actualValue).isEqualTo(testValue);
    }

    @Test
    public void set_theSettingThatWasSet_setsTheSetting() {
        String testKey = "fakeKey";
        String testValue = "fakeValue";
        EmitterSettings testSettings = spy(EmitterSettings.getInstance());

        testSettings.set(testKey, testValue);

        verify(testSettings).set(testKey, testValue);
        assertThat(testSettings.get(testKey)).isEqualTo(testValue);
    }

    @Test
    public void saveSettings() {
    }

    @Test
    public void getInstance_calledTwice_returnsSameObject() {
        EmitterSettings settingsFirstCall = EmitterSettings.getInstance();
        EmitterSettings settingsSecondCall = EmitterSettings.getInstance();

        assertThat(settingsFirstCall).isEqualTo(settingsSecondCall);
    }


    @Test
    public void  transferSettings_movesProperty_toHashMap() {
        String testKey = "fakeKey";
        String testValue = "fakeValue";
        EmitterSettings testSettings = spy(EmitterSettings.getInstance());
        Properties mockProps = new Properties();
        mockProps.setProperty(testKey, testValue);

        testSettings.transferSettings(mockProps);

        assertThat(testSettings.get(testKey)).isEqualTo(testValue);
        verify(testSettings).transferSettings(mockProps);
    }
}