package com.mikeapolis.neststats;

public class SettingNotFoundException extends RuntimeException {
    public SettingNotFoundException() {}

    public SettingNotFoundException(String msg) {
        super(msg);
    }
}
