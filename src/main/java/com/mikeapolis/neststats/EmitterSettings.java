package com.mikeapolis.neststats;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Container and source of truth for application settings.
 */
public class EmitterSettings {

    private final Logger LOG = LoggerFactory.getLogger(EmitterSettings.class);

    private static final String NSCONFIG = "nsconfig";
    private static final String DEFAULT_PATH = "not defined";   // TODO: Define this.
    private static final String CLASSPATH_CONFIG = "nest_api.properties";
    private static final String SETTINGS_DESCRIPTION = "Nest Stats Emitter Settings ";

    private static EmitterSettings instance = new EmitterSettings();
    private static ConcurrentHashMap<String, String> settings;

    private EmitterSettings() {
        settings = new ConcurrentHashMap<>();
        // Respect a filesystem config first...
        if (System.getProperty(NSCONFIG) != null ) {
            LOG.info("Loading settings from filesystem at {}.", System.getProperty(NSCONFIG));
            loadPropertiesFromFilesystem(new Properties());
        // ... but if the JVM was started without specifying one, use the one built into the classpath. It could be old.
        } else {
            LOG.info("Loading settings from the classpath shipped at build.");
            loadPropertiesFromBuildClasspath(new Properties());
        }
    }

    /**
     * Constructor helper method for loading properties from a file designated on the filesystem.
     *
     * @param props Properties to use for loading.
     */
    private void loadPropertiesFromFilesystem(Properties props) {
        String configPath = System.getProperty(NSCONFIG);
        try ( FileReader configReader = new FileReader(configPath) ) {
            LOG.info("Attempting to load properties from filesystem.");
            props.load(configReader);
        } catch (FileNotFoundException fnf) {
            throw new InvalidParameterException(String.format("Cannot find configuration file at {}.", configPath));
        } catch (IOException ioe) {
            throw new InvalidParameterException(String.format("Cannot load configuration due to {}.", ioe));
        }
        transferSettings(props);
        LOG.info("Properties loaded to settings.");
    }

    /**
     * Constructor helper method for loading default properties from the classpath provided by the build system.
     *
     * @param props Properties to use for loading.
     */
    private void loadPropertiesFromBuildClasspath(Properties props) {
        try (
                InputStream inStream = EmitterSettings.class.getClassLoader().getResourceAsStream(CLASSPATH_CONFIG)
        ) {
            LOG.info("Attempting to load properties from classpath.");
            props.load(inStream);
        } catch (IOException e) {
            LOG.error("Unable to access settings from classpath. {}", e);
            throw new InvalidParameterException("Cannot fallback to default settings in classpath.");
        }
        transferSettings(props);
        LOG.info("Properties loaded to settings.");
    }

    /**
     * Retrieve a setting.
     *
     * @param settingKey Name of the setting to be retrieved.
     * @return Value of the setting.
     */
    public synchronized String get(String settingKey) throws SettingNotFoundException {
        LOG.debug("Retrieving {}...", settingKey);
        if (!settings.containsKey(settingKey)) {
            LOG.error("{} not found.", settingKey);
            throw new SettingNotFoundException(String.format("Cannot retrieve {}. Not set?", settingKey));
        } else {
            String settingValue = settings.get(settingKey);
            if (settingValue == null) {
                LOG.warn("Found {}, but it was null.", settingKey);
            } else {
                LOG.debug("Found {}.", settingKey);
            }
            return settingValue;
        }
    }

    /**
     * Set or update a setting.
     *
     * @param settingKey The name of the setting.
     * @param settingValue The setting's value.
     */
    public synchronized void set(String settingKey, String settingValue) {
        settings.put(settingKey, settingValue);
        LOG.info("Added setting. {}: {}.", settingKey, settingValue);
    }

    /**
     * Commit the settings to disk. Morphs IOExceptions into RuntimeExceptions because we can still function with the
     * current settings even if we can't save them to disk.
     *
     * @throws RuntimeException
     */
    public synchronized void saveSettings() throws RuntimeException{
        String savePath;
        // Save to the filesystem if that was designated at runtime.
        if (System.getProperty(NSCONFIG) != null) {
            LOG.info("Saving settings to specified location: {}.", System.getProperty(NSCONFIG));
            savePath = System.getProperty(NSCONFIG);
        } else {
            LOG.info("Saving settings to default location: {}.", DEFAULT_PATH);
            savePath = DEFAULT_PATH;
        }
        long saveTimestamp = System.currentTimeMillis();
        try (FileWriter fw = new FileWriter(savePath)) {
            Properties props = new Properties();
            props.putAll(settings);
            props.store(fw, SETTINGS_DESCRIPTION + " " + saveTimestamp);
        } catch (IOException e) {
            LOG.error("Cannot persist settings to disk.");
            throw new RuntimeException(String.format("Unable to save settings due to {}.", e));
        }
    }

    /**
     * Transfer any properties into the settings map.
     *
     * @param props Properties to be loaded into settings.
     */
    protected void transferSettings(Properties props) {
            LOG.debug("Begin transferring properties to settings.");
            Set<String> defaultKeys = props.stringPropertyNames();
            for (String key : defaultKeys) {
                LOG.debug("Transferring {}.",key);
                settings.put(key, props.getProperty(key));
            }
            LOG.debug("End transferring properties to settings.");
    }

    /**
     * Access the settings instance that is greedily instantiated.
     *
     * @return singleton EmitterSettings instance.
     */
    public static EmitterSettings getInstance() {
        return instance;
    }
}