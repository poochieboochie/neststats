package com.mikeapolis.nest.api;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AccessTokenRequest extends ApiRequest {

    @JsonIgnore
    private final static String ENDPOINT = "https://api.home.nest.com/oauth2/access_token";

    private final String code;
    private final String grantType;

    public AccessTokenRequest (String authCode) {
        super(ENDPOINT);
        code = authCode;
        this.grantType = "authorization_code";
        buildJson(this);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName() + ": ");
        sb.append(super.toString());
        return sb.toString();
    }
}
