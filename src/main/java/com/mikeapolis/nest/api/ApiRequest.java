package com.mikeapolis.nest.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mikeapolis.neststats.EmitterSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ApiRequest {

    @JsonIgnore
    protected static final Logger LOG = LoggerFactory.getLogger(ApiRequest.class);

    @JsonIgnore
    private static final EmitterSettings SETTINGS = EmitterSettings.getInstance();

    @JsonIgnore
    private static final ObjectMapper MAPPER = NestObjectMapperFactory.createMapper();

    @JsonIgnore
    private final String endpoint;

    @JsonIgnore
    protected String payload;


    // Every API request requires these:
    protected String clientId;
    protected String clientSecret;

    public ApiRequest(String requestEndpoint) {
        clientId = SETTINGS.get("authorizationClientId");
        clientSecret = SETTINGS.get("authorizationClientSecret");
        endpoint = requestEndpoint;
    }

    protected void buildJson(ApiRequest requestType) {
        LOG.debug("Building {} with MAPPER: {}.", requestType, MAPPER);
        try {
            payload = MAPPER.writeValueAsString(requestType);
            LOG.debug("BUILT: " + payload);
        } catch (JsonProcessingException e) {
            LOG.error("Cannot build {}: {}: {}.", requestType.getClass().getSimpleName(), e, e.getMessage());
        }
    }

    @Override
    public String toString() {
        return this.payload;
    }
}
