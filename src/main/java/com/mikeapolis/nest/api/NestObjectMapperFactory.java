package com.mikeapolis.nest.api;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NestObjectMapperFactory {

    private static final Logger LOG = LoggerFactory.getLogger(NestObjectMapperFactory.class);

    private static ObjectMapper configure(ObjectMapper mapperToConfigure) {
        mapperToConfigure.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        LOG.debug("Setting field access to ANY for MAPPER: {}.", NestObjectMapperFactory.class);
        mapperToConfigure.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        LOG.debug("Using snake case PropertyNamingStrategy for MAPPER: {}.", NestObjectMapperFactory.class);
        return mapperToConfigure;
    }

    public static ObjectMapper createMapper() {
        ObjectMapper mapper = new ObjectMapper();
        return configure(mapper);
    }

}
