package com.mikeapolis.nest.api;

/**
 * A response from the Nest oauth2/access_token resource with an access token and expiration time.
 */
public class AccessTokenResponse {
    private final String accessToken;
    private final long expiresIn;       // Seconds

    public AccessTokenResponse(String accessToken, long expiresIn) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
    }

    /**
     * Accessor for access token.
     *
     * @return access token
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * Standard accessor for expiration time.
     *
     * @return seconds until expiration
     */
    public long getExpiresIn() {
        return expiresIn;
    }
}
